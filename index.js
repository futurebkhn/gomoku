const _ = require('lodash');
const path = require('path');
const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const config = {
    port: process.env.PORT || 3000,
};


app.set('view engine', 'ejs');
app.set('views', path.resolve(__dirname, 'ejs'));

app.use('/assets', require('express').static(path.resolve(__dirname, 'assets')));

app.get('/', async (req, res, next) => {
    res.render('home');
});

app.get('/room/:roomId', async (req, res, next) => {
    res.render('room');
});

// room socket io

const m = 20;
const numberOfSamePoint = 3;
const boardMap = {};

function createData() {
    let i, j, arr = [];
    for (i = 0; i < m; i++) {
        arr[i] = [];
        for (j = 0; j < m; j++) {
            arr[i][j] = 0;
        }
    }
    return arr;
}

io.of('/room').on('connection', (socket) => {

    socket.on('join', ({roomId, session}) => {
        socket.join(roomId);
        socket.roomId = roomId;
        socket.session = session;
        boardMap[roomId] = boardMap[roomId] || {
            count: 0,
            data: createData(),
            player1: '',
            player2: '',
            color: [],
            messages: [],
        };
        const roomInfo = boardMap[roomId];
        roomInfo.players = roomInfo.players || {}; // Khoi tao player = {} neu chua co
        roomInfo.players[session] = roomInfo.players[session] || {session: session}; //Khoi tao session = { } trong player neu chua co.
        const playerInfo = roomInfo.players[session]; //Khoi tao thong tin nguoi choi = session {}
        playerInfo.playerName = playerInfo.playerName || `P${Date.now().toString().substr(-10)}`; // Them playerName vao thong tin nguoi choi
        io.of('/room').in(roomId).emit('refreshBoard');

    });
    socket.on('getBoardInfo', (data, cb) => {
        const roomInfo = boardMap[socket.roomId];
        cb({
            count: roomInfo.count,
            data: roomInfo.data,
            playerName: roomInfo.players[socket.session].playerName,
            listPlayer: _.values(roomInfo.players),
            player1: roomInfo.player1,
            player2: roomInfo.player2,
            color: createObject(roomInfo.color),
            isWin: roomInfo.color.length > 0,
            messages: roomInfo.messages
        });
    });
    socket.on('changeUsername', ({playerName}) => {
        const roomInfo = boardMap[socket.roomId];
        const player = roomInfo.players[socket.session];
        player.playerName = playerName;
        io.of('/room').in(socket.roomId).emit('refreshBoard');
    });
    socket.on('setPlayer', ({type}) => {
        const roomInfo = boardMap[socket.roomId];
        if (roomInfo.count > 0) {
            return;
        }
        const player = roomInfo.players[socket.session];
        const currentPlayer = roomInfo['player' + type];
        const currentOtherPlayer = roomInfo['player' + (parseInt(type) === 1 ? 2 : 1)];
        if (currentPlayer) {
            if (currentPlayer.session === socket.session) {
                roomInfo['player' + type] = null;
                io.of('/room').in(socket.roomId).emit('refreshBoard');
            }
            return;
        }
        if (currentOtherPlayer && currentOtherPlayer.session === socket.session) {
            return;
        }
        roomInfo['player' + type] = player;
        io.of('/room').in(socket.roomId).emit('refreshBoard');
    });
    socket.on('touch', ({x, y}) => {
        const roomInfo = boardMap[socket.roomId];
        if (roomInfo.color.length) {
            return;
        }
        if (!roomInfo.player1 || !roomInfo.player2) {
            return;
        }
        if (roomInfo.count % 2 === 0 && roomInfo.player1 && roomInfo.player1.session === socket.session) {
            roomInfo.count++;
            roomInfo.data[x][y] = 1;
            const color = checkResult(roomInfo.data, x, y);
            if (color) {
                roomInfo.color = color;
            }
            io.of('/room').in(socket.roomId).emit('refreshBoard');

        }
        if (roomInfo.count % 2 === 1 && roomInfo.player2 && roomInfo.player2.session === socket.session) {
            roomInfo.count++;
            roomInfo.data[x][y] = 2;
            const color = checkResult(roomInfo.data, x, y);
            if (color) {
                roomInfo.color = color;
            }
            io.of('/room').in(socket.roomId).emit('refreshBoard');
        }
    });
    socket.on('newGame', () => {
        const roomInfo = boardMap[socket.roomId];
        if (!roomInfo.color.length) {
            return;
        }
        if (socket.session !== roomInfo.player1.session && socket.session !== roomInfo.player2.session) {
            return;
        }
        roomInfo.data = createData();
        roomInfo.color = [];
        roomInfo.count = 0;
        io.of('/room').in(socket.roomId).emit('refreshBoard');
    });
    socket.on('chat', ({message}) => {
        const roomInfo = boardMap[socket.roomId];
        const player = roomInfo.players[socket.session];
        roomInfo.messages.push({
            name: player.playerName,
            message: message
        });
        if (roomInfo.messages.length > 20) {
            roomInfo.messages.shift();
        }
        io.of('/room').in(socket.roomId).emit('refreshBoard');
    });
    socket.emit('ready');
});

function nextStep(row, column, srow, scolumn, times) {
    const nextRow = row + srow * times;
    const nextColumn = column + scolumn * times;
    if (m <= nextRow || nextRow < 0 || m <= nextColumn || nextColumn < 0) return null;
    else return {x: nextRow, y: nextColumn}

}

function checkDirections(data, row, column, srow, scolumn) {
    let t;
    const check = [{x: row, y: column}];
    const currentPointValue = data[row][column];
    let nextPoint;
    for (t = 1; t < 5; t++) {
        nextPoint = nextStep(row, column, srow, scolumn, t);
        if (!nextPoint || data[nextPoint.x][nextPoint.y] !== currentPointValue) {
            break;
        }
        check.push(nextPoint);
    }

    for (t = 1; t < 5; t++) {
        nextPoint = nextStep(row, column, -srow, -scolumn, t);
        if (!nextPoint || data[nextPoint.x][nextPoint.y] !== currentPointValue) {
            break;
        }
        check.push(nextPoint);
    }

    return check;
}

function checkResult(data, row, colum) {
    const directions = [{stepx: 0, stepy: 1}, {stepx: 1, stepy: 1}, {stepx: 1, stepy: 0}, {stepx: 1, stepy: -1}];
    for (let i = 0; i < directions.length; i++) {
        const d = directions[i];
        const check = checkDirections(data, row, colum, d.stepx, d.stepy);
        if (check.length === numberOfSamePoint) {
            return check;
        }
    }
    return false;
}

function createObject(color) {
    const m = {};
    let i, x, y;
    for (i = 0; i < color.length; i++) {
        x = color[i].x;
        y = color[i].y;
        if (!(x in m)) {
            m[x] = {};
        }
        m[x][y] = true;
    }
    return m;
}

server.listen(config.port, () => {
    console.info(`Server running on port ${config.port}`)
});
