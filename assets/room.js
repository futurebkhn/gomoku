iniSession();

const app = angular.module('app', []);

app.controller("gomoku", function ($scope) {


    let socket = io('/room');  //Step 1 : Connect to server
    $scope.data = [];


    socket.on('refreshBoard', () => {
        refreshBoard();
    });

    socket.on('ready', () => {
        socket.emit('join', {roomId: window.location.pathname, session: getCookie('session')});
        console.log('ready')
    });

    $scope.changeUsername = () => {
        let playerName = prompt("Please enter your name", "");
        if (!playerName) {
            return;
        }
        socket.emit('changeUsername', {playerName: playerName})
    };

    $scope.setPlayer = (type) => {
        socket.emit('setPlayer', {type: type})
    };

    $scope.touch = (x, y) => {
        socket.emit('touch', {x: x, y: y})
    };

    $scope.newGame = () => {
        socket.emit('newGame');
    };

    $scope.chat = () => {
        if (!$scope.message) {
            return;
        }
        socket.emit('chat', {message: $scope.message});
        $scope.message = '';
    };

    function refreshBoard() {
        socket.emit('getBoardInfo', null, (boardInfo) => {
            console.log('update board info', boardInfo);
            $scope.$evalAsync(() => {
                $scope.data = boardInfo.data;
                $scope.playerName = boardInfo.playerName;
                $scope.listPlayer = boardInfo.listPlayer;
                $scope.player1 = boardInfo.player1;
                $scope.player2 = boardInfo.player2;
                $scope.count = boardInfo.count;
                $scope.color = boardInfo.color;
                $scope.isWin = boardInfo.isWin;
                $scope.messages = boardInfo.messages;
            })
        })
    }
});

function iniSession() {
    const session = getCookie('session');
    if (session) {
        return session;
    }
    return setCookie('session', Date.now().toString(), 365);
}


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    return cvalue;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
