const m = 20;
"use strict";

function _2dArr() {
    let i, j, arr = [];
    for (i = 0; i < m; i++) {
        arr[i]=[];
        for (j = 0; j < m; j++) {
            arr[i][j] = 0;
        }
    }
    return arr;
}

let app = angular.module('gameGomoku',[]);

//Table

app.controller("tableController", function ($scope) {
    let data = _2dArr();
    $scope.data=data;
})




//Socket


let socket = io.connect();

socket.on('news', function (data) {
    console.log(data);
    socket.emit('my other event', {my: 'data'});
});